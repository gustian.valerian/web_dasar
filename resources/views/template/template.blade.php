<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css' rel='stylesheet'/>
    <title>Document</title>
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
        
          <div class="navbar-header">
            <!-- tombol membuka dropdown [media responsive] -->
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-1">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <!-- Nama Blog / website -->
            <a class="navbar-brand" href="#">Depan Komputer Developer</a>
          </div>
          
          <div class="collapse navbar-collapse" id="collapse-1">
            <ul class="nav navbar-nav">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#">Profile</a></li>
              <li><a href="#">User</a></li>
              {{-- <li class="dropdown">
                <!-- tombol membuka dropdown list-->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                Dropdown <span class="caret"></span>
                </a>
                <!-- dropdown list -->
                <ul class="dropdown-menu">
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 2</a></li>
                  <li><a href="#">Menu 3</a></li>
                  <li><a href="#">Menu 4</a></li>
                  <li><a href="#">Menu 5</a></li>
                </ul>
              </li> --}}
            </ul>
          </div>
          
        </div>
      </nav>

      <div class="container-fluid">
        @yield('body')
      </div>
</body>
</html>